console.log('*** JS Objects ***');

/*Javascript Objects

A data type that is used to represent real world objects
-Object literals {}
-Information are stored in key:value pair
-Key = properties

In JS, most core JS Features like Strings and Arrays are Objects
-Arrays are collection of data
-Strings are collection of characters
-Different data types maybe stored in an object's property creating compleex data structure
*/

let gradesArray = [98, 94, 89, 90];
let grades = {
	firstGrading: 98,
	secondGrading: 94,
	thirdGrading: 89,
	fourthGrading: 90
};
console.log(grades);

let user = {
	firsName: 'John',
	lastName: 'Doe',
	age: 25,
	location: {
		city: 'Tokyo,',
		country: 'Japan'
	},
	emails: [
		'john@mail.com',
		'johndoe@mail.com'
	],
	fullName: function(){
		return this.firstName + " " + this.lastName; 
	}
}
console.log(user);
console.log(user.fullName());

//Creating Objects
//1. usign object initializers/literal notation
/*This creates/declares an objects and also initializes/assign its properties upon
creation.
Syntax:
	let objectName = {
		keyA: valueA,
		keyB: valueB
	}
*/
let cellphone = {
	name: 'Nokia 3210',
	manufactureDate:1999
};

console.log('Result from using initializers')
console.log(cellphone);
console.log(typeof cellphone);

//2. using constructor function
/*Creates a reusable function to create several 
objects that have the same data structure
This usefule for creating multiple instance/copies
of an object
Syntax:
	function ObjectName(keyA, keyB){
		this.keyA = keyA;
		this.keyB = keyB;
	}

	let newObject = new ObjectName(keyA, keyB)
*/
//ex#1
function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
};

let laptop = new Laptop('Lenovo', 2008);
console.log('Result from using object constructors:');
console.log(laptop);

//ex#2
let myLaptop = new Laptop('Macbook Air', 2020);
console.log('Result from using object constructors:');
console.log(myLaptop);

//ex#3
let oldLaptop = Laptop('Portal R2E CCMC', 1980);
console.log("Result without the 'new' keyword");
console.log(oldLaptop) // this will result in undefined

//Creating empty object
let computer = {};
let myComputer = new Object();
console.log(computer);
console.log(myComputer);

//Accessing Objects Properties
//1. using the dot notation
console.log('Result from dot notation: ' + myLaptop.name);
//2. using the square bracket notation
console.log('Result from using square bracket: ' + myLaptop['name']);

//ex#2
let array = [laptop, myLaptop]
console.log(array[0]['name']);
console.log(array[0].name); //best practice of accessing objects

//Initializing Object Properties

let car = {} //empty

car.name = "Honda Civic";
console.log('Result from adding dot notation');
console.log(car);

car['manufacture date'] = 2019;
console.log(car['manufacture date']);
console.log(car['Manufacture Date']); //undefined
console.log(car.manufactureDate); //undefined
console.log('Result from using square bracket notation');
console.log(car);

//Deleting Object properties
delete car['manufacture date']; //pag may space ang square braket ang gagamitin
console.log('Result from deleting properties');
console.log(car);

//Reassigning properties
car.name = 'Dodge Charger R/T';
console.log('Result from reassigning properties');
console.log(car);

//Objects Methods

/* A method is a function which is a propery of an object
Similar to functions/features of a real world objects, methods
are defined based on what an object is capable of doing and how
it should work
*/
//ex#1
let person = {
	name: 'John',
	talk: function(){
		console.log('Hello my name is ' + this.name);

	}
}
console.log(person);
console.log('Result from Object method');
person.talk();
//ex#2
person.walk = function(){
	console.log(this.name + ' walked 25 steps forward.')
}
person.walk();
//ex#3
let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address: {
		city: 'Austin',
		country: 'Texas'
	},
	emails: [
	'joe@mail.com',
	'joesmith@mail.com'
	],
	phones: [
		'Android',
		'iPhone'
	],
	introduce: function(){
		console.log('Hello my name is ' + this.firstName + ' ' + this.lastName);
		console.log('This is my address ' + this.address.city + ', ' + this.address.country);
	}
}
friend.introduce();
console.log(friend.address.city)//pag tatawagin ang object sa loob ni object
console.log(friend.emails[0]); // pag tatawagin ang index 

/*Real World Application of Objects
-Scenario
1. We would like to create a game that would have several pokemon
interact with each other
2. Every pokemon would have the same set of stats, properties and
functions
*/

//Using object literals to create multiple kinds of pokemon would be time consuming
//ex#1 - ****USE CONSTRUCTOR NOT THIS****
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log('This Pokemon tackled targetPokemon');		
	},
	faint: function(){
		console.log("Pokemon fainter.")
	}
}
console.log(myPokemon);

//Creating an object constructor instead will help with this process
//ex#1
function Pokemon(name, level){
	//Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	//Methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is now reduced to _targetPokemonHealth_");		 
	},
	this.faint = function(){
		console.log(this.name + ' fainted.');
	}
} 
let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);